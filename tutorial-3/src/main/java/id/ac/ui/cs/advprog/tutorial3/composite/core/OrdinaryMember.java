package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {

    private String name;
    private String role;
    private Member member;
    private List<Member> childmember;
    public OrdinaryMember(String Name, String Role) {
        this.name =Name;
        this.role = Role;
        childmember = new ArrayList<Member>();
    }
    //TODO: Complete me

    @Override
    public List<Member> getChildMembers() {
        return this.childmember;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRole() {
        return this.role;
    }

    @Override
    public void addChildMember(Member member) {
        //do nothing
    }

    @Override
    public void removeChildMember(Member member) {
        //do nothing
    }

}
