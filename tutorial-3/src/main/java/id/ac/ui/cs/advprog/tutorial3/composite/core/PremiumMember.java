package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    private String name;
    private String role;
    private Member member;
    private List<Member> childmember;

    public PremiumMember(String Name, String Role) {
        this.name =Name;
        this.role = Role;
        childmember = new ArrayList<Member>();
    }
    //TODO: Complete me

    @Override
    public void addChildMember(Member member) {
        if(this.getRole().equals("Master")){
            this.childmember.add(member);
        }
        else if (this.childmember.size()<3 && !this.getRole().equals("Master") ) {
                this.childmember.add(member);
            }
        }


    @Override
    public void removeChildMember(Member member) {
        this.childmember.remove(member);
    }

    @Override
    public String getRole() {
        return this.role;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public List<Member> getChildMembers() {
        return this.childmember;
    }
}
