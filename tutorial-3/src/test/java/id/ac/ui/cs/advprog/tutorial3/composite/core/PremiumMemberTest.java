package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        assertEquals("Aqua",member.getName());

        //TODO: Complete me
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Goddess",member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member mem = new PremiumMember("Aququ", "Tuhan");
        member.addChildMember(mem);
        assertEquals(true,member.getChildMembers().contains(mem));
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member mem = new PremiumMember("Aququ", "Tuhan");
        member.addChildMember(mem);
        assertEquals(true,member.getChildMembers().contains(mem));
        member.removeChildMember(mem);
        assertEquals(false,member.getChildMembers().contains(mem));
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member mem = new PremiumMember("Aququ", "Tuhan");
        for (int i =0;i<=4; i++){
                member.addChildMember(mem);
        }
        assertEquals(3,member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member guildmaster = new PremiumMember("Aququ", "Master");
        Member mem = new PremiumMember("Aququ", "Tuhan");
        for (int i =0;i<=3; i++){
            guildmaster.addChildMember(mem);
        }
        assertEquals(4,guildmaster.getChildMembers().size());
    }
}
